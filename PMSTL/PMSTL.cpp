// PMSTL.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <utility>
#include "pmc_algorithm.h"
#include "pmc_utility.h"
#include "KlasaTest1.h"
#include "TestFunctors.h"
#include <algorithm>
#include <vector>

//todo: nie uzywaj class, uzywaj typename w szablonach. I pomysl o skroceniu nazw typow, bo troche dlugie sa te deklaracje. Moze jakos to mozna fajniej zrobic. 
//todo: move it to some test package and call package.test or something
#include "Test_pmc_is_permutation.h"
#include "Test_pmc_find.h"
#include "Test_pmc_mismatch.h"
#include "Test_pmc_count.h"
#include "Test_pmc_adjacent_find.h"
#include "Test_pmc_count_if.h"
#include "Test_pmc_find_if.h"
#include "Test_pmc_find_if_not.h"
#include "Test_pmc_find_first_of.h"
#include "Test_pmc_search.h"

//docelowo to musi zniknac stad, podobnie zreszta jak wiele innych
#include "TestClass1_Functors.h" 

KlasaTest1& makeKlasaTest1(){
	KlasaTest1 *k = new KlasaTest1(11);
	return (*k);
	//return KlasaTest1(11);
}

int main()
{
	if (!(Test_pmc_find().test()) ||
		!(Test_pmc_is_permutation().test()) ||
		!(Test_pmc_mismatch().test()) ||
		!(Test_pmc_count().test()) ||
		!(Test_pmc_adjacent_find().test()) ||
		!(Test_pmc_count_if().test()) ||
		!(Test_pmc_find_if().test()) ||
		!(Test_pmc_find_if_not().test()) ||
		!(Test_pmc_find_first_of().test()) ||
		!(Test_pmc_search().test())
		) {
		int key;
		std::cin >> key;
		return 1;
	}

	KlasaTest1 k1(1, "a");
	KlasaTest1 k2(2, "b");
	KlasaTest1 k3(3, "c");
	KlasaTest1 k4(4, "d");
	KlasaTest1 k5(5, "e");
	KlasaTest1 k1_1(1, "f");

	//auto k = pmstl::min(k1, k2, klasaTest1FirstIsLess);
	//auto k = pmstl::min(k1, k6, klasaTest1FirstIsLess);
	//std::cout << k.toString() << "\n";
	
	std::vector<KlasaTest1> v;
	v.reserve(6);
	v.push_back(k5);
	v.push_back(k1_1);
	v.push_back(k3);
	v.push_back(k1);
	v.push_back(k4);
	v.push_back(k2);

	std::vector<int> v2;
	v2.push_back(1);
	v2.push_back(1);
	v2.push_back(3);
	v2.push_back(4);
	v2.push_back(5);
	TestClass::Functors::TestFunctorKt functor1t;
	TestClass::Functors::SumKt1 functor1s;
	//int i;
	//TestFunctor functor2 = pmstl::for_each(v.begin(), v.end(), functor1);
	//TestFunctor functor2 = std::for_each(v2.begin(), v2.end(), functor1);
	//Sum functor2s = pmstl::for_each(v.begin(), v.end(), functor1s);
	TestClass::Functors::TestFunctorKt functor2t = pmstl::for_each(v.begin(), v.end(), functor1t);
	//auto functor2 = pmstl::for_each(v.begin(), v.end(), functor1);
	//int i = functor2.count;
	//int i = functor2s.sum;
	//i = functor2t.count;
	
	//std::cout << "Ile: " << pmstl::for_each(v.begin(), v.end(), functor1t).count << "\n";
	//std::cout << "Ile: " << *(pmstl::find_if(v2.begin(), v2.end(), EvenFunctorInt())) << "\n";
	//auto it = pmstl::find_if(v.begin(), v.end(), EvenFunctorKt1 ());
	//int i = (*it).i;
	//std::cout << "Ile: " << i << "\n"  << "\n";
	
	//std::cout << "Ile: " << pmstl::count_if(v.begin(), v.end(), EvenFunctorKt1()) << "\n" << "\n";
	std::cout << "Ile: " << pmstl::count(v.begin(), v.end(), k1) << "\n";

	/*
	auto p1 = pmstl::minmax_element(v.begin(), v.end());

	if (p1.first == v.end()) {
		std::cout << "v.end()\n";
	}
	else {
		std::cout << (*(p1.first)).i << " " << (*(p1.first)).name << "\n";
	}
	if (p1.second == v.end()) {
		std::cout << "v.end()\n";
	}
	else {
		std::cout << (*(p1.second)).i << " " << (*(p1.second)).name << "\n";
	}
	*/

	std::vector<int> v3;
	v3.reserve(5);
	v3.push_back(1);
	v3.push_back(2);
	v3.push_back(3);
	v3.push_back(3);
	v3.push_back(5);

	//auto it = pmstl::adjacent_find(v3.begin(), v3.end());
	auto it = pmstl::search_n(v3.begin(), v3.end(), 3, 3);
	if (it == v3.end()) {
		std::cout << "Nie Znalazlem: "<< "\n";
	}
	else {
		std::cout << "Znalazlem: " << *it << "\n";
	}

	for (int i = 0; i < 3; i++) {
		KlasaTest1 k1(1, "a");
		std::cout << k1.toString();
	}
	//std::find_first_of
	int key;
	std::cin >> key;
    return 0;
}

