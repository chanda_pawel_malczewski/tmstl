#pragma once
#include "AbstractTest.h"
#include "pmc_mismatch.h"
#include <algorithm>

class Test_pmc_mismatch : AbstractTest
{
public:
	bool test();
protected:
	std::string getTestName();
private:
	bool test001();
	bool test002();
	bool test003();
	bool test004();
	bool test101();
	bool test102();
	bool test201();

	template<typename ForwardIterator1, typename ForwardIterator2> bool testSameResults(ForwardIterator1 first1, ForwardIterator1 last1, ForwardIterator2 first2) {
		auto stdRes = std::mismatch(first1, last1, first2);
		auto pmstlRes = pmstl::mismatch(first1, last1, first2);
		return pairsAreEquals(stdRes, pmstlRes);
	}
	template<typename ForwardIterator1, typename ForwardIterator2, typename BinaryPredicate> bool testSameResultsWithPredicate(ForwardIterator1 first1, ForwardIterator1 last1, ForwardIterator2 first2, BinaryPredicate pred) {
		auto stdRes = std::mismatch(first1, last1, first2, pred);
		auto pmstlRes = pmstl::mismatch(first1, last1, first2, pred);
		return pairsAreEquals(stdRes, pmstlRes);
	}
	
};

