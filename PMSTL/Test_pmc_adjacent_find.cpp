#include "stdafx.h"
#include "Test_pmc_adjacent_find.h"
#include <vector>
#include "KlasaTest1.h"
#include "TestClass1_Functors.h"
#include "TestFunctors.h"

typedef std::vector<int> vintT;
typedef std::vector<KlasaTest1> vKT1T;

std::string Test_pmc_adjacent_find::getTestName()
{
	return "adjacent_find";
}

bool Test_pmc_adjacent_find::test() {
	bool tmpRes = true;

	if (!test001()) {
		printTestFailure("test01");
		tmpRes = false;
	}

	if (!test002()) {
		printTestFailure("test02");
		tmpRes = false;
	}

	if (!test003()) {
		printTestFailure("test03");
		tmpRes = false;
	}

	if (!test004()) {
		printTestFailure("test04");
		tmpRes = false;
	}

	if (!test101()) {
		printTestFailure("test101");
		tmpRes = false;
	}

	if (!test201()) {
		printTestFailure("test201");
		tmpRes = false;
	}

	if (!test202()) {
		printTestFailure("test202");
		tmpRes = false;
	}

	if (!test203()) {
		printTestFailure("test203");
		tmpRes = false;
	}

	if (tmpRes) {
		printTestPassed();
	}

	return tmpRes;
}

bool Test_pmc_adjacent_find::test001()
{
	vintT v1;

	return testSameResults(v1.begin(), v1.end());
}

bool Test_pmc_adjacent_find::test002()
{
	vintT v1;
	v1.push_back(1);
	v1.push_back(2);
	v1.push_back(3);

	return testSameResults(v1.begin(), v1.end());
}

bool Test_pmc_adjacent_find::test003()
{
	vintT v1;
	v1.push_back(1);
	v1.push_back(2);
	v1.push_back(2);

	return testSameResults(v1.begin(), v1.end());
}

bool Test_pmc_adjacent_find::test004()
{
	vintT v1;
	v1.push_back(1);
	v1.push_back(2);
	v1.push_back(2);
	v1.push_back(1);
	v1.push_back(1);

	return testSameResults(v1.begin(), v1.end());
}

bool Test_pmc_adjacent_find::test101()
{
	vKT1T v1;
	v1.push_back(KlasaTest1(1));
	v1.push_back(KlasaTest1(2));
	v1.push_back(KlasaTest1(1));
	v1.push_back(KlasaTest1(3));
	v1.push_back(KlasaTest1(3));
	v1.push_back(KlasaTest1(1));
	v1.push_back(KlasaTest1(1));

	return testSameResults(v1.begin(), v1.end());
}
//==============
bool Test_pmc_adjacent_find::test201()
{
	vintT v1;

	return testSameResultsPred(v1.begin(), v1.end(), TestIntEqualFunctor());
}

bool Test_pmc_adjacent_find::test202()
{
	vintT v1;
	v1.push_back(1);
	v1.push_back(2);
	v1.push_back(3);
	v1.push_back(1);
	v1.push_back(3);

	return testSameResultsPred(v1.begin(), v1.end(), TestIntEqualFunctor());
}

bool Test_pmc_adjacent_find::test203()
{
	vintT v1;
	v1.push_back(1);
	v1.push_back(2);
	v1.push_back(2);
	v1.push_back(1);
	v1.push_back(1);

	return testSameResultsPred(v1.begin(), v1.end(), TestIntEqualFunctor());
}