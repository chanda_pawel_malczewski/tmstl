#pragma once
#include "AbstractTest.h"
#include "pmc_search.h"
#include <algorithm>

class Test_pmc_search : AbstractTest
{
public:
	bool test();
protected:
	std::string getTestName();
private:
	bool test001();
	bool test002();
	bool test003();
	bool test004();
	bool test005();
	bool test006();
	bool test101();
	bool test102();
	bool test201();

	template<typename ForwardIterator1, typename ForwardIterator2> bool testSameResults(ForwardIterator1 first1, ForwardIterator1 last1, ForwardIterator2 first2, ForwardIterator2 last2) {
		auto stdRes = std::search(first1, last1, first2, last2);
		auto pmstlRes = pmstl::search(first1, last1, first2, last2);
		return pmstlRes == stdRes;
	}

	//todo: tu akurat jest dobrze z iteratorami, ale pilnuj tego wszedzie. Ale upewnij si� te�, ze indeksowanie typ�w jest u�yte tylko je�li rzeczywi�cie jest to potrzebne. 
	template<typename ForwardIterator1, typename ForwardIterator2, typename BinaryPredicate> bool testSameResultsPred(ForwardIterator1 first1, ForwardIterator1 last1, ForwardIterator2 first2, ForwardIterator2 last2, BinaryPredicate pred) {
		auto stdRes = std::search(first1, last1, first2, last2, pred);
		auto pmstlRes = pmstl::search(first1, last1, first2, last2, pred);
		return pmstlRes == stdRes;
	}
};
