#pragma once
//#ifndef KLASATEST1_H
//#define KLASATEST1_H


#include <string>
#include <sstream>
#include <iostream>
#include "stdafx.h"

class KlasaTest1 {
public:
	static int id_sequence;
	int id;
	int i;
	std::string name;
	KlasaTest1(const KlasaTest1 & kt):name(kt.name), i(kt.i), id(++id_sequence) {
		//std::cout << "KlasaTest1 CC: id, i, str " << id << " " <<kt.i<<" "<<kt.name<<"\n";
	}

	KlasaTest1(const int &_i, const std::string &_name): i(_i), name(_name), id(++id_sequence) {
		//std::cout << "KlasaTest1 CP: id, i, str " <<id<<" "<< _i << " " << _name << "\n";
	}

	KlasaTest1(const int &_i) : i(_i), name("no name"), id(++id_sequence) {
		//std::cout << "KlasaTest1 CPi: id, i, str " << id << " " << _i << " " << "no name" << "\n";
	}

	virtual ~KlasaTest1() { 
		//std::cout << "KlasaTest1 Destructor: "<<id<<"\n"; 
	}

	KlasaTest1 & operator = (const KlasaTest1 & source) {
		//std::cout << "KlasaTest1 OP = \n";
		if (this != &source)
		{
			//std::cout << "this != &source\n";
			this->name = source.name;
			this->i = source.i;
		}
		else {
			//std::cout << "this == &source\n";
		}
		return *this;
	}
	/*
	bool operator == (const KlasaTest1 & other) {
		return (this.i == other.i);
	}*/
/*
	bool operator < (const KlasaTest1 & kt) {
		return this->i < kt.i;
	}

	bool operator < (KlasaTest1 & kt) {
		return this->i < kt.i;
	}

	bool operator < (KlasaTest1 kt) {
		return this->i < kt.i;
	}
	*/
	std::string toString() const{
		std::stringstream ss;
		ss <<"id: "<<id<<" i: "<<i<<" name: " <<name << "\n";
		return std::string(ss.str());
	}
};

//int KlasaTest1::id_sequence = 0;

/*
KlasaTest1 & operator = (const KlasaTest1 & desc, const KlasaTest1 & source) {
	if (&desc != &source)
	{
		desc.name = source.name;
		desc.i = source.i;
	}
	return desc;

}*/


inline bool operator < (const KlasaTest1& k1, const KlasaTest1& k2) {
	return (k1.i < k2.i);
}

inline bool operator == (const KlasaTest1 & k1, const KlasaTest1 & k2) {
	return k1.i == k2.i;
}

inline bool klasaTest1FirstIsLess(const KlasaTest1 & k1, const KlasaTest1 & k2) {
	return (k1 < k2);
	//return k1.i < k2.i;
}

//#endif