#include "stdafx.h"
#include "AbstractTest.h"
#include <iostream>

void AbstractTest::printTestFailure(std::string methodName)
{
	std::cout << "TEST FAILURE: " << getTestName() <<  " Method: " << methodName << "\n";
}

void AbstractTest::printTestPassed()
{
	std::cout <<  "TEST PASSED: " << getTestName() << "\n";
}
