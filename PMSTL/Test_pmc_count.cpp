#include "stdafx.h"
#include "Test_pmc_count.h"
#include <vector>
#include "KlasaTest1.h"
#include "TestClass1_Functors.h"
#include "TestFunctors.h"

typedef std::vector<int> vintT;
typedef std::vector<KlasaTest1> vKT1T;

std::string Test_pmc_count::getTestName()
{
	return "count";
}

bool Test_pmc_count::test() {
	bool tmpRes = true;

	//todo: troche ten kod sie powtarza. Trzebaby doda� do klasy bazowej jak�� list� i dodawa� na koniec kolejne testy. Poni�ej rozpoznanie
	//this->_member = &Test_pmc_count::test004;
	//(this->*_member)();
	
	if (!test001()) {
		printTestFailure("test01");
		tmpRes = false;
	}

	if (!test002()) {
		printTestFailure("test02");
		tmpRes = false;
	}

	if (!test003()) {
		printTestFailure("test03");
		tmpRes = false;
	}

	if (!test004()) {
		printTestFailure("test04");
		tmpRes = false;
	}

	if (!test101()) {
		printTestFailure("test101");
		tmpRes = false;
	}

	if (!test201()) {
		printTestFailure("test201");
		tmpRes = false;
	}

	if (!test202()) {
		printTestFailure("test202");
		tmpRes = false;
	}

	if (!test203()) {
		printTestFailure("test203");
		tmpRes = false;
	}

	if (!test204()) {
		printTestFailure("test204");
		tmpRes = false;
	}

	if (!test301()) {
		printTestFailure("test301");
		tmpRes = false;
	}


	if (tmpRes) {
		printTestPassed();
	}

	return tmpRes;
}

bool Test_pmc_count::test001()
{
	vintT v1;

	return testSameResults(v1.begin(), v1.end(), 1);
}

bool Test_pmc_count::test002()
{
	vintT v1;
	v1.push_back(1);
	v1.push_back(2);
	v1.push_back(3);

	return testSameResults(v1.begin(), v1.end(), 1);
}

bool Test_pmc_count::test003()
{
	vintT v1;
	v1.push_back(3);
	v1.push_back(2);
	v1.push_back(1);

	return testSameResults(v1.begin(), v1.end(), 1);
}

bool Test_pmc_count::test004()
{
	vintT v1;
	v1.push_back(1);
	v1.push_back(2);
	v1.push_back(1);

	return testSameResults(v1.begin(), v1.end(), 1);
}

bool Test_pmc_count::test101()
{
	vKT1T v1;
	v1.push_back(KlasaTest1(1));
	v1.push_back(KlasaTest1(2));
	v1.push_back(KlasaTest1(1));

	return testSameResults(v1.begin(), v1.end(), KlasaTest1(1));
}
//==============
bool Test_pmc_count::test201()
{
	vintT v1;

	return testSameResultsPred(v1.begin(), v1.end(), 1, TestIntEqualFunctor(), 0);
}

bool Test_pmc_count::test202()
{
	vintT v1;
	v1.push_back(1);
	v1.push_back(2);
	v1.push_back(3);

	return testSameResultsPred(v1.begin(), v1.end(), 1, TestIntEqualFunctor(), 1);
}

bool Test_pmc_count::test203()
{
	vintT v1;
	v1.push_back(3);
	v1.push_back(2);
	v1.push_back(1);

	return testSameResultsPred(v1.begin(), v1.end(), 1, TestIntEqualFunctor(), 1);
}

bool Test_pmc_count::test204()
{
	vintT v1;
	v1.push_back(1);
	v1.push_back(2);
	v1.push_back(1);

	return testSameResultsPred(v1.begin(), v1.end(), 1, TestIntEqualFunctor(), 2);
}

bool Test_pmc_count::test301()
{
	vKT1T v1;
	v1.push_back(KlasaTest1(1));
	v1.push_back(KlasaTest1(2));
	v1.push_back(KlasaTest1(1));

	return testSameResultsPred(v1.begin(), v1.end(), KlasaTest1(1), TestClass::Functors::EqualFunctorKt1(),2);
}
