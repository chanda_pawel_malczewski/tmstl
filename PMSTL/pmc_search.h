#pragma once
namespace pmstl {
	//todo: dodaj wersje, kt�ra szuka ci�gu n razy. Teraz mo�esz szuka� ci�gu albo szuka� n wyst�pie� warto�ci pod rz�d. ale nie mo�esz szuka� n wyst�pie� ci�gu. 
	//jeszcze pomysle gdzie to dorzuce, bo pasuje do obu. W zasadzie pasowaloby search_n rozszerzyc o iteratory, a szukanie pojedynczej wartosci zaimplementowac np jako wska�nik na wartosc (begin) i wskaznik na nastepny hipotetyczny element(end). Ale nie ma sensu.
	//z drugiej strony szukanie zakresu to szukanie zakresu raz :D Ale narazie to oleje calkowicie. Wydaje mi sie troche mniej potrzebne. 
	//moze dodam jako calkowicie nowa funkcje. 
	//a mo�e wog�le tego nie pisa�. Bo mo�e zamiast tego przyda�aby si� funkcja ktora duplikuje dany ci�g n razy. No ma to wad� w postaci zaj�to�ci pami�ci, konstruktory. Zobacze jeszcze. 
	//akurat to spokojnie mo�e by� na d�ugiej li�cie todo.

	template <class ForwardIterator1, class ForwardIterator2> ForwardIterator1 search(ForwardIterator1 first1, ForwardIterator1 last1, ForwardIterator2 first2, ForwardIterator2 last2) {
		if (first2 == last2) return first1;  // specified in C++11

		if (first1 == last1) return last1;
		//if (first2 == last2) return last1; //w tym przypadku zwracamy first zamiast last. Mozna to rozumowac tak: skoro szukamy pustego ciagu, to wszedzie on jest. 

		while (first1 != last1) {
			ForwardIterator1 tmpFirst1 = first1;
			ForwardIterator2 tmpFirst2 = first2;

			while (*tmpFirst1 == *tmpFirst2) {
				++tmpFirst2;
				if (tmpFirst2 == last2)
					return first1;//czyli poczatek ciagu. nie tmpFirst2!!
				++tmpFirst1;
				if (tmpFirst1 == last1) return last1;//bo to znaczy ze drugi zakres jest wiekszy wiec nigdy nie bedzie w pierwszym
			}
			++first1;
		}
		return last1;
	}

	template <class ForwardIterator1, class ForwardIterator2, class BinaryPredicate> ForwardIterator1 search(ForwardIterator1 first1, ForwardIterator1 last1, ForwardIterator2 first2, ForwardIterator2 last2, BinaryPredicate pred) {
		if (first1 == last1) return last1;
		if (first2 == last2) return last1;

		while (first1 != last1) {
			ForwardIterator1 tmpFirst1 = first1;
			ForwardIterator2 tmpFirst2 = first2;

			while (pred(*tmpFirst1, *tmpFirst2)) {
				++tmpFirst2;
				if (tmpFirst2 == last2)
					return first1;//czyli poczatek ciagu. nie tmpFirst2!!
				++tmpFirst1;
				if (tmpFirst1 == last1) return last1;//bo to znaczy ze drugi zakres jest wiekszy wiec nigdy nie bedzie w pierwszym
			}
			++first1;
		}
		return last1;
	}
}//namespace
