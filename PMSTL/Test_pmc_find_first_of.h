#pragma once
#include "AbstractTest.h"
#include "pmc_find_first_of.h"
#include <algorithm>

class Test_pmc_find_first_of : AbstractTest
{
public:
	bool test();
protected:
	std::string getTestName();
private:
	bool test001();
	bool test002();
	bool test003();
	bool test004();
	bool test005();
	bool test006();
	bool test101();
	bool test102();
	bool test201();

	template<typename ForwardIterator, typename InputIterator> bool testSameResults(ForwardIterator first1, ForwardIterator last1, InputIterator first2, InputIterator last2) {
		auto stdRes = std::find_first_of(first1, last1, first2, last2);
		auto pmstlRes = pmstl::find_first_of(first1, last1, first2, last2);
		return pmstlRes == stdRes;
	}

	//todo: tu akurat jest dobrze z iteratorami, ale pilnuj tego wszedzie. Ale upewnij si� te�, ze indeksowanie typ�w jest u�yte tylko je�li rzeczywi�cie jest to potrzebne. 
	template<typename ForwardIterator, typename InputIterator, typename UnaryPredicate> bool testSameResultsPred(ForwardIterator first1, ForwardIterator last1, InputIterator first2, InputIterator last2, UnaryPredicate pred) {
		auto stdRes = std::find_first_of(first1, last1, first2, last2, pred);
		auto pmstlRes = pmstl::find_first_of(first1, last1, first2, last2, pred);
		return pmstlRes == stdRes;
	}
};
