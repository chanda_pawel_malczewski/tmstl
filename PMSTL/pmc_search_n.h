#pragma once
namespace pmstl {
	//todo: zmien to na forme tak� jak w refrencji
	//todo: dodaj class Size count
	//todo: potestuj dla warunkow brzegowych
	//http://www.cplusplus.com/reference/algorithm/search_n/ tu jest ciekawe.
	template <class ForwardIterator, class T> ForwardIterator search_n(ForwardIterator first, ForwardIterator last, int count, const T &value) {
		while (first != last) {
			if (*first == value) {
				ForwardIterator tmpIt = first;
				++tmpIt;
				for (int i = 0; i < count - 1; i++) {
					if (tmpIt == last) {
						return last;//bo nie znajdziemy, za krotki zakres
					}

					if (!(*tmpIt == value))
						break;//for
					if (i == count - 1 - 1)//last is still equal
						return first;
					++tmpIt;//same, test next
				}
			}
			++first;
		}
		return last;
	}

	template <class ForwardIterator, class T, class BinaryPreditate> ForwardIterator search_n(ForwardIterator first, ForwardIterator last, int count, const T &value, BinaryPreditate pred) {
		while (first != last) {
			if (*first == value) {
				ForwardIterator tmpIt = first;
				++tmpIt;
				for (int i = 0; i < count - 1; i++) {
					if (tmpIt == last) {
						return last;//bo nie znajdziemy, za krotki zakres
					}

					if (!pred(*tmpIt, value))
						break;//for
					if (i == count - 1 - 1)//last is still equal
						return first;
					++tmpIt;//same, test next
				}
			}
			++first;
		}
		return last;
	}
}//namespace