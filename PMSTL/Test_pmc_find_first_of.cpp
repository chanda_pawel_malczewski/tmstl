#include "stdafx.h"
#include "Test_pmc_find_first_of.h"
#include <vector>
#include "KlasaTest1.h"
//todo: nie zawsze to jest potrzebne, a i tak moze naglowek klasy testowej powinien to inkludowac?
//#include "TestClass1_Functors.h"
#include "TestFunctors.h"

typedef std::vector<int> vintT;
typedef std::vector<KlasaTest1> vKT1T;

std::string Test_pmc_find_first_of::getTestName()
{
	return "find_first_of";
}

bool Test_pmc_find_first_of::test() {
	bool tmpRes = true;

	if (!test001()) {
		printTestFailure("test01");
		tmpRes = false;
	}

	if (!test002()) {
		printTestFailure("test02");
		tmpRes = false;
	}

	if (!test003()) {
		printTestFailure("test03");
		tmpRes = false;
	}

	if (!test004()) {
		printTestFailure("test04");
		tmpRes = false;
	}
	
	if (!test005()) {
		printTestFailure("test05");
		tmpRes = false;
	}

	if (!test006()) {
		printTestFailure("test06");
		tmpRes = false;
	}


	if (!test101()) {
		printTestFailure("test101");
		tmpRes = false;
	}

	if (!test102()) {
		printTestFailure("test102");
		tmpRes = false;
	}

	if (!test201()) {
		printTestFailure("test201");
		tmpRes = false;
	}

	if (tmpRes) {
		printTestPassed();
	}

	return tmpRes;
}

bool Test_pmc_find_first_of::test001()
{
	vintT v1;
	vintT v2;

	return testSameResults(v1.begin(), v1.end(), v2.begin(), v2.end());
}

bool Test_pmc_find_first_of::test002()
{
	vintT v1;
	vintT v2;
	v2.push_back(1);
	v2.push_back(2);
	v2.push_back(3);

	return testSameResults(v1.begin(), v1.end(), v2.begin(), v2.end());
}

bool Test_pmc_find_first_of::test003()
{
	vintT v1;
	v1.push_back(1);
	v1.push_back(2);
	v1.push_back(3);

	vintT v2;

	return testSameResults(v1.begin(), v1.end(), v2.begin(), v2.end());
}

bool Test_pmc_find_first_of::test004()
{
	vintT v1;
	v1.push_back(1);
	v1.push_back(2);
	v1.push_back(3);
	vintT v2;
	v2.push_back(4);
	v2.push_back(5);
	v2.push_back(6);

	return testSameResults(v1.begin(), v1.end(), v2.begin(), v2.end());
}

bool Test_pmc_find_first_of::test005()
{
	vintT v1;
	v1.push_back(1);
	v1.push_back(2);
	v1.push_back(3);
	vintT v2;
	v2.push_back(4);
	v2.push_back(1);
	v2.push_back(6);

	return testSameResults(v1.begin(), v1.end(), v2.begin(), v2.end());
}

bool Test_pmc_find_first_of::test006()
{
	vintT v1;
	v1.push_back(1);
	v1.push_back(2);
	v1.push_back(3);
	vintT v2;
	v2.push_back(3);
	v2.push_back(2);
	v2.push_back(1);

	return testSameResults(v1.begin(), v1.end(), v2.begin(), v2.end());
}

bool Test_pmc_find_first_of::test101()
{
	vKT1T v1;
	v1.push_back(KlasaTest1(1));
	v1.push_back(KlasaTest1(2));
	v1.push_back(KlasaTest1(3));
	vKT1T v2;
	v2.push_back(KlasaTest1(4));
	v2.push_back(KlasaTest1(5));
	v2.push_back(KlasaTest1(6));

	return testSameResults(v1.begin(), v1.end(), v2.begin(), v2.end());
}

bool Test_pmc_find_first_of::test102()
{
	vKT1T v1;
	v1.push_back(KlasaTest1(1));
	v1.push_back(KlasaTest1(2));
	v1.push_back(KlasaTest1(3));
	vKT1T v2;
	v2.push_back(KlasaTest1(3));
	v2.push_back(KlasaTest1(2));
	v2.push_back(KlasaTest1(1));

	return testSameResults(v1.begin(), v1.end(), v2.begin(), v2.end());
}


bool Test_pmc_find_first_of::test201()
{
	vintT v1;
	v1.push_back(1);
	v1.push_back(2);
	v1.push_back(3);
	vintT v2;
	v2.push_back(3);
	v2.push_back(2);
	v2.push_back(1);

	return testSameResultsPred(v1.begin(), v1.end(), v2.begin(), v2.end(), TestIntEqualFunctor());
}