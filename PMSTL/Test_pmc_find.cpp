#include "stdafx.h"
#include "Test_pmc_find.h"
#include <vector>
#include "KlasaTest1.h"
#include "TestClass1_Functors.h"

typedef std::vector<int> vintT;
typedef std::vector<KlasaTest1> vKT1T;
std::string Test_pmc_find::getTestName()
{
	return "find";
}

bool Test_pmc_find::test() {
	bool tmpRes = true;

	if (!test001()) {
		printTestFailure("test01");
		tmpRes = false;
	}

	if (!test002()) {
		printTestFailure("test02");
		tmpRes = false;
	}

	if (!test003()) {
		printTestFailure("test03");
		tmpRes = false;
	}
	
	if (!test004()) {
		printTestFailure("test04");
		tmpRes = false;
	}

	if (!test101()) {
		printTestFailure("test101");
		tmpRes = false;
	}

	if (!test102()) {
		printTestFailure("test102");
		tmpRes = false;
	}

	if (!test103()) {
		printTestFailure("test103");
		tmpRes = false;
	}

	if (!test201()) {
		printTestFailure("test201");
		tmpRes = false;
	}

	if (!test202()) {
		printTestFailure("test202");
		tmpRes = false;
	}

	if (!test203()) {
		printTestFailure("test203");
		tmpRes = false;
	}

	if (tmpRes) {
		printTestPassed();
	}

	return tmpRes;
}

bool Test_pmc_find::test001()
{
	vintT v1;
	
	return testSameResults(v1.begin(), v1.end(),1);
}

bool Test_pmc_find::test002()
{
	vintT v1;
	v1.push_back(1);
	v1.push_back(2);
	v1.push_back(3);

	return testSameResults(v1.begin(), v1.end(), 1);
}

bool Test_pmc_find::test003()
{
	vintT v1;
	v1.push_back(3);
	v1.push_back(2);
	v1.push_back(1);

	return testSameResults(v1.begin(), v1.end(), 1);
}

bool Test_pmc_find::test004()
{
	vintT v1;
	v1.push_back(3);
	v1.push_back(2);
	v1.push_back(5);

	return testSameResults(v1.begin(), v1.end(), 1);
}

bool Test_pmc_find::test101()
{
	vKT1T v1;
	v1.push_back(KlasaTest1(1));
	v1.push_back(KlasaTest1(2));
	v1.push_back(KlasaTest1(1));

	return testSameResults(v1.begin(), v1.end(), KlasaTest1(1));
}

bool Test_pmc_find::test102()
{
	vKT1T v1;
	v1.push_back(KlasaTest1(1));
	v1.push_back(KlasaTest1(1));
	v1.push_back(KlasaTest1(1));

	return testSameResults(v1.begin(), v1.end(), KlasaTest1(1));
}

bool Test_pmc_find::test103()
{
	vKT1T v1;
	v1.push_back(KlasaTest1(1));
	v1.push_back(KlasaTest1(2));
	v1.push_back(KlasaTest1(3));

	return testSameResults(v1.begin(), v1.end(), KlasaTest1(4));
}

bool Test_pmc_find::test201()
{
	vKT1T v1;
	v1.push_back(KlasaTest1(1, "a"));
	v1.push_back(KlasaTest1(2, "b"));
	v1.push_back(KlasaTest1(3, "c"));

	vKT1T::iterator foundedIterator = pmstl::pmstlex::find(v1.begin(), v1.end(), KlasaTest1(4, "d"), TestClass::Functors::EqualFunctorKt1());
	return foundedIterator == v1.end();
}


bool Test_pmc_find::test202()
{
	vKT1T v1;
	v1.push_back(KlasaTest1(3, "c"));
	v1.push_back(KlasaTest1(2, "b"));
	v1.push_back(KlasaTest1(1, "a"));

	vKT1T::iterator foundedIterator = pmstl::pmstlex::find(v1.begin(), v1.end(), KlasaTest1(1, "d"), TestClass::Functors::EqualFunctorKt1());
	return (*foundedIterator).name == "a";
}

bool Test_pmc_find::test203()
{
	vKT1T v1;
	v1.push_back(KlasaTest1(1,"a"));
	v1.push_back(KlasaTest1(1,"b"));
	v1.push_back(KlasaTest1(1,"c"));

	vKT1T::iterator foundedIterator = pmstl::pmstlex::find(v1.begin(), v1.end(), KlasaTest1(1,"d"), TestClass::Functors::EqualFunctorKt1());
	return (*foundedIterator).name == "a";
}
