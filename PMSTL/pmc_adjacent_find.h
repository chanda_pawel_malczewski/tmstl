#pragma once

namespace pmstl {
	template <class ForwardIterator> ForwardIterator adjacent_find(ForwardIterator first, ForwardIterator last) {
		if (first == last) {
			return last;
		}
		ForwardIterator next = first;
		++next;
		while (next != last) {
			if (*first == *next)
				return first;
			++first;
			++next;
		}

		return last;
	}

	//todo: sprawdz czy zewnetrzny operator == jest takim funktorem
	template <class ForwardIterator, class BinaryPred > ForwardIterator adjacent_find(ForwardIterator first, ForwardIterator last, BinaryPred pred) {
		if (first == last) {
			return last;
		}
		ForwardIterator next = first;
		++next;
		while (next != last) {
			if (pred(*first, *next))
				return first;
			++first;
			++next;
		}

		return last;
	}
}//namespace