#pragma once
#include "AbstractTest.h"
#include "pmc_find.h"

class Test_pmc_find: AbstractTest
{
public:
	bool test();
protected:
	std::string getTestName();
private:
	bool test001();
	bool test002();
	bool test003();
	bool test004();
	bool test101();
	bool test102();
	bool test103();
	bool test201();
	bool test202();
	bool test203();
	
	template<typename ForwardIterator1, typename ValueT> bool testSameResults(ForwardIterator1 first, ForwardIterator1 last, const ValueT & searchedVal) {
		auto stdRes = std::find(first, last, searchedVal);
		auto pmstlRes = pmstl::find(first, last, searchedVal);
		return stdRes == pmstlRes;
	}
	
	//there is no std::find with predicate to comapre with
	/*
	template<typename ForwardIterator1, typename UnaryPred> bool testSameResultsPred(ForwardIterator1 first1, ForwardIterator1 last1, ForwardIterator2 first2, UnaryPred pred) {
		bool stdRes = std::is_permutation(first1, last1, first2, pred);
		bool pmstlRes = pmstl::is_permutation(first1, last1, first2, pred);
		return stdRes == pmstlRes;
	}*/
};

