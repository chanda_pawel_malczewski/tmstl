#pragma once

namespace pmstl {
	//todo: replate int with iterator_traits<InputIterator>::difference_type
	template <class ForwardIterator, class T2> int count(ForwardIterator first, ForwardIterator last, const T2 & likeValue) {
		int tmpCount = 0;
		while (first != last) {
			if ((*first) == likeValue)
				++tmpCount;
			++first;
		}
		return tmpCount;
	}
	namespace pmstlex {
		//todo: replate int with iterator_traits<InputIterator>::difference_type
		template <class ForwardIterator, class T2, typename BinaryPred > int count(ForwardIterator first, ForwardIterator last, const T2 & likeValue, BinaryPred pred) {
			int tmpCount = 0;
			while (first != last) {
				if (pred((*first), likeValue))
					++tmpCount;
				++first;
			}
			return tmpCount;
		}
	}
}