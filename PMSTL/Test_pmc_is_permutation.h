#pragma once
#include "AbstractTest.h"

//todo: make it pure static class
class Test_pmc_is_permutation: AbstractTest
{
public:
	bool test();
protected:
	std::string getTestName();
private:
	//==== for int =====
	//empty (first = end)
	bool test01();
	//one elements
	bool test02();
	bool test03();
	bool test04();
	bool test05();
	bool test06();
	bool test07();
	bool test08();
	bool test09();
	bool test10();
	bool test11();
	//==== for class =====
	bool test12();
	bool test13();
	//=== for predicate ====
	bool test14();
	bool test15();


	template<typename ForwardIterator1, typename ForwardIterator2> bool testSameResults(ForwardIterator1 first1, ForwardIterator1 last1, ForwardIterator2 first2) {
		bool stdRes = std::is_permutation(first1, last1, first2);
		bool pmstlRes = pmstl::is_permutation(first1, last1, first2);
		return stdRes == pmstlRes;
	}
	template<typename ForwardIterator1, typename ForwardIterator2, typename BinaryPred> bool testSameResultsPred(ForwardIterator1 first1, ForwardIterator1 last1, ForwardIterator2 first2, BinaryPred pred) {
		bool stdRes = std::is_permutation(first1, last1, first2, pred);
		bool pmstlRes = pmstl::is_permutation(first1, last1, first2, pred);
		return stdRes == pmstlRes;
	}
};

