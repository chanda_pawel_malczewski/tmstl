#include "stdafx.h"
#include "Test_pmc_count_if.h"
#include <vector>
#include "KlasaTest1.h"
#include "TestClass1_Functors.h"
#include "TestFunctors.h"

typedef std::vector<int> vintT;
typedef std::vector<KlasaTest1> vKT1T;

std::string Test_pmc_count_if::getTestName()
{
	return "count_if";
}

bool Test_pmc_count_if::test() {
	bool tmpRes = true;

	if (!test001()) {
		printTestFailure("test01");
		tmpRes = false;
	}

	if (!test002()) {
		printTestFailure("test02");
		tmpRes = false;
	}

	if (!test003()) {
		printTestFailure("test03");
		tmpRes = false;
	}

	if (!test004()) {
		printTestFailure("test04");
		tmpRes = false;
	}

	if (!test101()) {
		printTestFailure("test101");
		tmpRes = false;
	}

	if (tmpRes) {
		printTestPassed();
	}

	return tmpRes;
}

bool Test_pmc_count_if::test001()
{
	vintT v1;

	return testSameResultsPred(v1.begin(), v1.end(), EvenFunctorInt());
}

bool Test_pmc_count_if::test002()
{
	vintT v1;
	v1.push_back(1);
	v1.push_back(1);
	v1.push_back(3);

	return testSameResultsPred(v1.begin(), v1.end(), EvenFunctorInt());
}

bool Test_pmc_count_if::test003()
{
	vintT v1;
	v1.push_back(2);
	v1.push_back(2);
	v1.push_back(1);

	return testSameResultsPred(v1.begin(), v1.end(), EvenFunctorInt());
}

bool Test_pmc_count_if::test004()
{
	vintT v1;
	v1.push_back(2);
	v1.push_back(4);
	v1.push_back(6);

	return testSameResultsPred(v1.begin(), v1.end(), EvenFunctorInt());
}

bool Test_pmc_count_if::test101()
{
	vKT1T v1;
	v1.push_back(KlasaTest1(1));
	v1.push_back(KlasaTest1(2));
	v1.push_back(KlasaTest1(3));
	v1.push_back(KlasaTest1(4));
	v1.push_back(KlasaTest1(5));
	v1.push_back(KlasaTest1(6));

	return testSameResultsPred(v1.begin(), v1.end(), TestClass::Functors::EvenFunctorKt1());
}
