#pragma once
namespace pmstl {
	template <class ForwardIterator, class Pred> ForwardIterator find_if(ForwardIterator first, ForwardIterator last, Pred pred) {
		while (first != last) {
			if (pred(*first))
				return first;
			++first;
		}
		return last;
	}
}//namespace