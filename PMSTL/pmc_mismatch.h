#pragma once
#include "pmc_utility.h"

namespace pmstl {
	//todo: w doc jest InputIterator. Zbadaj
	template <class ForwardIterator1, class ForwardIterator2> pair<ForwardIterator1, ForwardIterator2> mismatch(ForwardIterator1 first1, ForwardIterator1 last1, ForwardIterator2 first2) {
		while (first1 != last1) {
			if (!(*first1 == *first2))//this condition can be moved into while
				return make_pair<ForwardIterator1, ForwardIterator2>(first1, first2);
			++first1;
			++first2;
		}
		return make_pair<ForwardIterator1, ForwardIterator2>(first1, first2);
	}

	template <class ForwardIterator1, class ForwardIterator2, class BinaryPredicate> pair<ForwardIterator1, ForwardIterator2> mismatch(ForwardIterator1 first1, ForwardIterator1 last1, ForwardIterator2 first2, BinaryPredicate pred) {
		while (first1 != last1) {
			if (!pred(*first1, *first2))//this condition can be moved into while
				return make_pair<ForwardIterator1, ForwardIterator2>(first1, first2);
			++first1;
			++first2;
		}
		return make_pair<ForwardIterator1, ForwardIterator2>(first1, first2);
	}
}//namespace