#pragma once
#include "AbstractTest.h"
#include "pmc_count.h"
#include <algorithm>

class Test_pmc_count : AbstractTest
{
public:
	bool test();
	typedef bool(Test_pmc_count::*MemberMethodPointer)(void);
	MemberMethodPointer _member;
protected:
	std::string getTestName();
private:
	bool test001();
	bool test002();
	bool test003();
	bool test004();
	bool test101();

	bool test201();
	bool test202();
	bool test203();
	bool test204();
	bool test301();
	
	template<typename ForwardIterator1, typename ValueT> bool testSameResults(ForwardIterator1 first, ForwardIterator1 last, const ValueT & searchedVal) {
		auto stdRes = std::count(first, last, searchedVal);
		auto pmstlRes = pmstl::count(first, last, searchedVal);
		return stdRes == pmstlRes;
	}

	//todo: change int into diff
	template<typename ForwardIterator1, typename ValueT, typename BinayPredicate> bool testSameResultsPred(ForwardIterator1 first, ForwardIterator1 last, const ValueT & searchedVal, BinayPredicate pred, int expectedRes) {
		auto pmstlRes = pmstl::pmstlex::count(first, last, searchedVal, pred);
		return pmstlRes == expectedRes;
	}
};

