#pragma once
#include "KlasaTest1.h"

//todo: extract it
//todo: add namespace
struct EvenFunctorInt
{
	EvenFunctorInt() { }
	bool operator() (const int &i) {
		return  ((i % 2) == 0);
	}
};

struct TestIntEqualFunctor
{
	TestIntEqualFunctor() { }
	bool operator() (const int &i1, const int &i2) {
		return (i1 == i2);
	}
};
