#include "stdafx.h"
#include "Test_pmc_is_permutation.h"

//pmstl
#include "pmc_is_permutation.h"

//std
#include <algorithm>
#include <vector>
#include <iostream>

//test utils
#include "KlasaTest1.h"
#include "TestClass1_Functors.h"

std::string Test_pmc_is_permutation::getTestName() {
	return "is_permutation";
}

bool Test_pmc_is_permutation::test()
{
	bool tmpRes = true;

	if (!test01()) {
		printTestFailure("test01");
		tmpRes = false;
	}

	if (!test02()) {
		printTestFailure("test02");
		tmpRes = false;
	}

	if (!test03()) {
		printTestFailure("test03");
		tmpRes = false;
	}

	if (!test04()) {
		printTestFailure("test04");
		tmpRes = false;
	}

	if (!test05()) {
		printTestFailure("test05");
		tmpRes = false;
	}

	if (!test06()) {
		printTestFailure("test06");
		tmpRes = false;
	}

	if (!test07()) {
		printTestFailure("test07");
		tmpRes = false;
	}

	if (!test08()) {
		printTestFailure("test08");
		tmpRes = false;
	}

	if (!test09()) {
		printTestFailure("test09");
		tmpRes = false;
	}

	if (!test10()) {
		printTestFailure("test10");
		tmpRes = false;
	}

	if (!test11()) {
		printTestFailure("test11");
		tmpRes = false;
	}

	if (!test12()) {
		printTestFailure("test12");
		tmpRes = false;
	}

	if (!test13()) {
		printTestFailure("test13");
		tmpRes = false;
	}

	if (!test14()) {
		printTestFailure("test13");
		tmpRes = false;
	}

	if (!test15()) {
		printTestFailure("test13");
		tmpRes = false;
	}

	if (tmpRes) {
		printTestPassed();
	}

	return tmpRes;
}

bool Test_pmc_is_permutation::test01()
{
	std::vector<int> v1;
	std::vector<int> v2;
	return testSameResults(v1.begin(), v1.end(), v2.begin());
}

bool Test_pmc_is_permutation::test02()
{
	std::vector<int> v1;
	v1.push_back(1);
	std::vector<int> v2;
	v2.push_back(1);
	return testSameResults(v1.begin(), v1.end(), v2.begin());
}

bool Test_pmc_is_permutation::test03()
{
	std::vector<int> v1;
	v1.push_back(1);
	std::vector<int> v2;
	v2.push_back(2);
	return testSameResults(v1.begin(), v1.end(), v2.begin());
}

bool Test_pmc_is_permutation::test04()
{
	std::vector<int> v1;
	v1.push_back(1);
	v1.push_back(2);
	std::vector<int> v2;
	v2.push_back(2);
	v2.push_back(1);
	return testSameResults(v1.begin(), v1.end(), v2.begin());
}

bool Test_pmc_is_permutation::test05()
{
	std::vector<int> v1;
	v1.push_back(1);
	v1.push_back(3);
	std::vector<int> v2;
	v2.push_back(2);
	v2.push_back(1);
	return testSameResults(v1.begin(), v1.end(), v2.begin());
}

bool Test_pmc_is_permutation::test06()
{
	std::vector<int> v1;
	v1.push_back(1);
	v1.push_back(1);
	v1.push_back(2);
	std::vector<int> v2;
	v2.push_back(2);
	v2.push_back(1);
	v2.push_back(1);
	return testSameResults(v1.begin(), v1.end(), v2.begin());
}

bool Test_pmc_is_permutation::test07()
{
	std::vector<int> v1;
	v1.push_back(1);
	v1.push_back(1);
	v1.push_back(3);
	std::vector<int> v2;
	v2.push_back(2);
	v2.push_back(1);
	v2.push_back(1);
	return testSameResults(v1.begin(), v1.end(), v2.begin());
}

bool Test_pmc_is_permutation::test08()
{
	std::vector<int> v1;
	v1.push_back(1);
	v1.push_back(2);
	v1.push_back(3);
	v1.push_back(4);
	v1.push_back(5);
	std::vector<int> v2;
	v2.push_back(1);
	v2.push_back(2);
	v2.push_back(5);
	v2.push_back(4);
	v2.push_back(3);
	return testSameResults(v1.begin(), v1.end(), v2.begin());
}

bool Test_pmc_is_permutation::test09()
{
	std::vector<int> v1;
	v1.push_back(1);
	v1.push_back(2);
	v1.push_back(3);
	v1.push_back(4);
	v1.push_back(5);
	std::vector<int> v2;
	v2.push_back(1);
	v2.push_back(2);
	v2.push_back(5);
	v2.push_back(7);
	v2.push_back(3);
	return testSameResults(v1.begin(), v1.end(), v2.begin());
}

bool Test_pmc_is_permutation::test10()
{
	std::vector<int> v1;
	v1.push_back(1);
	v1.push_back(2);
	v1.push_back(3);
	v1.push_back(4);
	v1.push_back(3);
	std::vector<int> v2;
	v2.push_back(1);
	v2.push_back(2);
	v2.push_back(3);
	v2.push_back(3);
	v2.push_back(4);
	return testSameResults(v1.begin(), v1.end(), v2.begin());
}

bool Test_pmc_is_permutation::test11()
{
	std::vector<int> v1;
	v1.push_back(1);
	v1.push_back(2);
	v1.push_back(3);
	v1.push_back(4);
	v1.push_back(3);
	std::vector<int> v2;
	v2.push_back(1);
	v2.push_back(2);
	v2.push_back(3);
	v2.push_back(3);
	v2.push_back(5);
	return testSameResults(v1.begin(), v1.end(), v2.begin());
}

bool Test_pmc_is_permutation::test12()
{
	std::vector<KlasaTest1> v1;
	v1.push_back(KlasaTest1(1));
	v1.push_back(KlasaTest1(2));
	v1.push_back(KlasaTest1(3));
	std::vector<KlasaTest1> v2;
	v2.push_back(KlasaTest1(3));
	v2.push_back(KlasaTest1(1));
	v2.push_back(KlasaTest1(2));
	return testSameResults(v1.begin(), v1.end(), v2.begin());
}

bool Test_pmc_is_permutation::test13()
{
	std::vector<KlasaTest1> v1;
	v1.push_back(KlasaTest1(1));
	v1.push_back(KlasaTest1(2));
	v1.push_back(KlasaTest1(3));
	std::vector<KlasaTest1> v2;
	v2.push_back(KlasaTest1(3));
	v2.push_back(KlasaTest1(4));
	v2.push_back(KlasaTest1(2));
	return testSameResults(v1.begin(), v1.end(), v2.begin());
}

bool Test_pmc_is_permutation::test14()
{
	std::vector<KlasaTest1> v1;
	v1.push_back(KlasaTest1(1));
	v1.push_back(KlasaTest1(2));
	v1.push_back(KlasaTest1(3));
	std::vector<KlasaTest1> v2;
	v2.push_back(KlasaTest1(4));
	v2.push_back(KlasaTest1(6));
	v2.push_back(KlasaTest1(5));
	return testSameResultsPred(v1.begin(), v1.end(), v2.begin(), TestClass::Functors::EqualFunctorKt1Mod3());
}

bool Test_pmc_is_permutation::test15()
{
	std::vector<KlasaTest1> v1;
	v1.push_back(KlasaTest1(1));
	v1.push_back(KlasaTest1(2));
	v1.push_back(KlasaTest1(3));
	std::vector<KlasaTest1> v2;
	v2.push_back(KlasaTest1(3));
	v2.push_back(KlasaTest1(4));
	v2.push_back(KlasaTest1(3));
	return testSameResultsPred(v1.begin(), v1.end(), v2.begin(), TestClass::Functors::EqualFunctorKt1Mod3());
}
