#pragma once
namespace pmstl {
	//todo: replate int with iterator_traits<InputIterator>::difference_type
	template<class ForwardIterator, class Pred> int count_if(ForwardIterator first, ForwardIterator last, Pred pred) {
		int tmpCount = 0;
		while (first != last) {
			if (pred(*first))
				++tmpCount;
			++first;
		}
		return tmpCount;
	}
}//namespace