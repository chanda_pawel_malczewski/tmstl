#pragma once
#include "AbstractTest.h"
#include "pmc_search_n.h"
#include <algorithm>

class Test_pmc_search_n : AbstractTest
{
public:
	bool test();
protected:
	std::string getTestName();
private:
	bool test001();
	bool test002();
	bool test003();
	bool test004();
	bool test005();
	bool test006();
	bool test101();
	bool test102();
	bool test201();

	template<typename ForwardIterator, typename T> bool testSameResults(ForwardIterator first, ForwardIterator last, int count, const T &value) {
		auto stdRes = std::search_n(first, last, count, value);
		auto pmstlRes = pmstl::search_n(first, last, count, value);
		return pmstlRes == stdRes;
	}

	//todo: tu akurat jest dobrze z iteratorami, ale pilnuj tego wszedzie. Ale upewnij si� te�, ze indeksowanie typ�w jest u�yte tylko je�li rzeczywi�cie jest to potrzebne. 
	template<typename ForwardIterator1, typename ForwardIterator2, typename BinaryPredicate> bool testSameResultsPred(ForwardIterator first, ForwardIterator last, int count, const T &value, BinaryPredicate pred) {
		auto stdRes = std::search_n(first, last, count, value, pred);
		auto pmstlRes = pmstl::search_n(first, last, count, value, pred);
		return pmstlRes == stdRes;
	}
};