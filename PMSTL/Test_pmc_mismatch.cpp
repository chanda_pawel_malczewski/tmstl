#include "stdafx.h"
#include "Test_pmc_mismatch.h"
#include <vector>
#include "KlasaTest1.h"
#include "TestClass1_Functors.h"

typedef std::vector<int> vintT;
typedef std::vector<KlasaTest1> vKT1T;
std::string Test_pmc_mismatch::getTestName()
{
	return "mismatch";
}

bool Test_pmc_mismatch::test() {
	bool tmpRes = true;

	if (!test001()) {
		printTestFailure("test01");
		tmpRes = false;
	}

	if (!test002()) {
		printTestFailure("test02");
		tmpRes = false;
	}

	if (!test003()) {
		printTestFailure("test03");
		tmpRes = false;
	}

	if (!test004()) {
		printTestFailure("test04");
		tmpRes = false;
	}

	if (!test101()) {
		printTestFailure("test101");
		tmpRes = false;
	}

	if (!test102()) {
		printTestFailure("test102");
		tmpRes = false;
	}

	if (!test201()) {
		printTestFailure("test201");
		tmpRes = false;
	}

	if (tmpRes) {
		printTestPassed();
	}

	return tmpRes;
}

bool Test_pmc_mismatch::test001()
{
	vintT v1;
	vintT v2;
	return testSameResults(v1.begin(), v1.end(), v2.begin());
}

bool Test_pmc_mismatch::test002()
{
	vintT v1;
	v1.push_back(1);
	v1.push_back(2);
	v1.push_back(3);

	vintT v2;
	v2.push_back(1);
	v2.push_back(2);
	v2.push_back(3);

	return testSameResults(v1.begin(), v1.end(), v2.begin());
}

bool Test_pmc_mismatch::test003()
{
	vintT v1;
	v1.push_back(1);
	v1.push_back(2);
	v1.push_back(3);

	vintT v2;
	v2.push_back(0);
	v2.push_back(2);
	v2.push_back(3);

	return testSameResults(v1.begin(), v1.end(), v2.begin());
}

bool Test_pmc_mismatch::test004()
{
	vintT v1;
	v1.push_back(1);
	v1.push_back(2);
	v1.push_back(3);
	
	vintT v2;
	v2.push_back(1);
	v2.push_back(2);
	v2.push_back(4);

	return testSameResults(v1.begin(), v1.end(), v2.begin());
}

bool Test_pmc_mismatch::test101()
{
	vKT1T v1;
	v1.push_back(KlasaTest1(1, "a"));
	v1.push_back(KlasaTest1(2, "b"));
	v1.push_back(KlasaTest1(3, "c"));

	vKT1T v2;
	v2.push_back(KlasaTest1(1, "a"));
	v2.push_back(KlasaTest1(2, "b"));
	v2.push_back(KlasaTest1(3, "c"));

	return testSameResults(v1.begin(), v1.end(), v2.begin());
}

bool Test_pmc_mismatch::test102()
{
	vKT1T v1;
	v1.push_back(KlasaTest1(1, "a"));
	v1.push_back(KlasaTest1(4, "d"));
	v1.push_back(KlasaTest1(3, "c"));

	vKT1T v2;
	v2.push_back(KlasaTest1(1, "a"));
	v2.push_back(KlasaTest1(2, "b"));
	v2.push_back(KlasaTest1(3, "c"));

	return testSameResults(v1.begin(), v1.end(), v2.begin());
}

bool Test_pmc_mismatch::test201()
{
	vKT1T v1;
	v1.push_back(KlasaTest1(1, "a"));
	v1.push_back(KlasaTest1(4, "d"));
	v1.push_back(KlasaTest1(3, "c"));

	vKT1T v2;
	v2.push_back(KlasaTest1(1, "a"));
	v2.push_back(KlasaTest1(2, "b"));
	v2.push_back(KlasaTest1(3, "c"));

	return testSameResultsWithPredicate(v1.begin(), v1.end(), v2.begin(), TestClass::Functors::EqualFunctorKt1());
}