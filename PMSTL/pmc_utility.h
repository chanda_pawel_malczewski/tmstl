#pragma once
namespace pmstl {
	template <class T1, class T2> class pair {

	public:
		typedef T1 first_type;
		typedef T2 second_type;
		typedef pair<T1, T2> _Myt;

		first_type first;
		second_type second;

		virtual ~pair() { 
			//std::cout << "pair Destructor\n"; 
		}

		pair(const first_type & _first, const second_type &_second) : first(_first), second(_second)
		{
			//std::cout << "pair CP\n";
		}

		//tutaj odpowiem na pytanie dlaczego to musi byc w "liscie inicjujacej". Otoz minmax przyjmuje referencje i zwraca pare refrencji. 
		//wiec pair.first jest referencj�. Sta�� nawiasem m�wi�c. A takie co� musi by� zainicjowane w miejscu definicji. Czyli
		//w przypadku zwyklych zmiennych w miejscu a w przypadku klas na tych listach wlasnie.
		/*template <class U1, class U2> */pair(const pair & p) : first(p.first), second(p.second) {
			//std::cout << "pair CC\n";
			/*
			this->first = pair.first;
			this->second = pair.first;
			*/
		}
		
		//typ zwracany nie musi byc wprost podant (_Myt VS). Ale ju� typ �r�d�a tak, przy czym uwaga... to nie musi by� identyczny
		//typ z docelow� par�. Wystarczy �eby by� konwertowalny. Pobawimy si� potem. Mo�e napisze konstruktor ktory przyjmuje inta. 
		template <class U1, class U2> pair & operator = (const pair<U1, U2> & source) {
			//std::cout << "pair operator =\n";
			//if (this != &source)//tego tu byc nie moze chyba ze na void* bo moga byc niezgodne typy
			this->first = source.first;
			this->second = source.second;
			return *this;
		}
	};

	template <typename T1, typename T2> pair<T1 , T2 > inline make_pair(T1 a, T2 b) {
		//std::cout << "make pair\n";
		return (pair<T1 , T2>(a, b)); //pytanie: potrzeba te typy? nie wydedukuje sam?
	}
}