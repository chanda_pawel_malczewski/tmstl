#pragma once
#include "KlasaTest1.h"
namespace TestClass {
	namespace Functors {
		class TestFunctorKt {
		public:
			TestFunctorKt() :count(0) {};
			int count;
			void operator() (const KlasaTest1 &k) {
				if ((k.i % 2) == 0) {
					++count;
				}
			}
		};

		struct SumKt1
		{
			SumKt1() : sum{ 0 } { }
			//void operator()(int n) { sum += n; }
			void operator() (const KlasaTest1 &k) {
				if ((k.i % 2) == 0) {
					++sum;
				}
			}
			int sum;
		};

		struct EvenFunctorKt1
		{
			EvenFunctorKt1() { }
			bool operator() (const KlasaTest1 &k) {
				return  ((k.i % 2) == 0);
			}
		};


		struct EqualFunctorKt1
		{
			EqualFunctorKt1() { }
			bool operator() (const KlasaTest1 &k1, const KlasaTest1 &k2) {
				return  (k1.i == k2.i);
			}
		};

		struct EqualFunctorKt1Mod3
		{
			EqualFunctorKt1Mod3() { }
			bool operator() (const KlasaTest1 &k1, const KlasaTest1 &k2) {
				return  ((k1.i % 3) == (k2.i % 3));
			}
		};
	}//namespace Functors
}//namespace TestClass