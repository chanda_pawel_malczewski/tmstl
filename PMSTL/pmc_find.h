#pragma once

namespace pmstl {
	template <class ForwardIterator, class T2> ForwardIterator find(ForwardIterator first, ForwardIterator last, const T2 & likeVal) {
		while (first != last) {
			if ((*first) == likeVal)
				return first;
			++first;
		}
		return last;
	}

	namespace pmstlex {
		template <class ForwardIterator, class T2, class BinaryPred> ForwardIterator find(ForwardIterator first, ForwardIterator last, const T2 & likeVal, BinaryPred pred) {
			while (first != last) {
				if (pred((*first), likeVal))
					return first;
				++first;
			}
			return last;
		}
	}
}