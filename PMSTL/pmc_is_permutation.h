#pragma once
#include "pmc_find.h"
#include "pmc_count.h"
#include "pmc_mismatch.h"

namespace pmstl {
	template <typename ForwardIterator1, typename ForwardIterator2> bool is_permutation(ForwardIterator1 first1, ForwardIterator1 last1, ForwardIterator2 first2) {
		//pmstl::pair<ForwardIterator1, ForwardIterator2> p = pmstl::mismatch(first1, last1, first2);
		auto p = pmstl::mismatch(first1, last1, first2);
		first1 = p.first;
		first2 = p.second;
		if (first1 == last1) {
			return true;
		}
		//todo: use pmstl
		ForwardIterator2 last2 = first2;
		std::advance(last2, std::distance(first1, last1));

		ForwardIterator1 tmpFirst1 = first1;
		ForwardIterator2 tmpFirst2 = first2;

		while (tmpFirst1 != last1) {
			//each element I test once
			if (pmstl::find(first1, tmpFirst1, *tmpFirst1) == tmpFirst1) {
				//int c1 = pmstl::count(tmpFirst1, last1, *tmpFirst1);
				int c2 = pmstl::count(first2, last2, *tmpFirst1);
				if ((c2 == 0) || (c2 != pmstl::count(tmpFirst1, last1, *tmpFirst1))) {
					return false;
				}
			}

			++tmpFirst1;
			++tmpFirst2;
		}
		return true;
	}

	template <typename ForwardIterator1, typename ForwardIterator2, typename BinaryPredicate> bool is_permutation(ForwardIterator1 first1, ForwardIterator1 last1, ForwardIterator2 first2, BinaryPredicate pred) {
		//pmstl::pair<ForwardIterator1, ForwardIterator2> p = pmstl::mismatch(first1, last1, first2);
		auto p = pmstl::mismatch(first1, last1, first2, pred);
		first1 = p.first;
		first2 = p.second;
		if (first1 == last1) {
			return true;
		}
		//todo: use pmstl
		ForwardIterator2 last2 = first2;
		std::advance(last2, std::distance(first1, last1));

		ForwardIterator1 tmpFirst1 = first1;
		ForwardIterator2 tmpFirst2 = first2;

		while (tmpFirst1 != last1) {
			//each element I test once
			if (pmstl::pmstlex::find(first1, tmpFirst1, *tmpFirst1, pred) == tmpFirst1) {
				//int c1 = pmstl::count(tmpFirst1, last1, *tmpFirst1);
				int c2 = pmstl::pmstlex::count(first2, last2, *tmpFirst1, pred);
				if ((c2 == 0) || (c2 != pmstl::pmstlex::count(tmpFirst1, last1, *tmpFirst1, pred))) {
					return false;
				}
			}

			++tmpFirst1;
			++tmpFirst2;
		}
		return true;
	}
}