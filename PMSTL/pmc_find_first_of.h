#pragma once
#include "pmc_find.h"

namespace pmstl {
	template <class ForwardIterator1, class ForwardIterator2> ForwardIterator1 find_first_of(ForwardIterator1 first1, ForwardIterator1 last1,
		ForwardIterator2 first2, ForwardIterator2 last2) {

		if (first1 == last1)
			return last1;
		if (first2 == last2) {
			return last1;
		}
		while (first1 != last1) {
			if (pmstl::find(first2, last2, *first1) != last2)
				return first1;
			++first1;
		}
		return last1;
	}

	template <class ForwardIterator1, class ForwardIterator2, class BinaryPred> ForwardIterator1 find_first_of(ForwardIterator1 first1, ForwardIterator1 last1,
		ForwardIterator2 first2, ForwardIterator2 last2, BinaryPred pred) {

		if (first1 == last1)
			return last1;
		if (first2 == last2) {
			return last1;
		}
		while (first1 != last1) {
			if (pmstl::pmstlex::find(first2, last2, *first1, pred) != last2)
				return first1;
			++first1;
		}
		return last1;
	}
}//namespace
