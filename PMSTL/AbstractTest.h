#pragma once
#include <string>
class AbstractTest {
public:
	virtual bool test() = 0;
protected:
	void printTestFailure(std::string methodName);
	void printTestPassed();
	virtual std::string getTestName() = 0;
//todo: change_name
	template<typename Pair1, typename Pair2> bool pairsAreEquals(Pair1 pair1, Pair2 pair2) {
		if (!(pair1.first == pair2.first))
			return false;
		if (!(pair1.second == pair2.second))
			return false;
		return true;
	}
};