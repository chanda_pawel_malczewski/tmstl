#pragma once
#include "AbstractTest.h"
#include "pmc_find_if_not.h"
#include <algorithm>

class Test_pmc_find_if_not : AbstractTest
{
public:
	bool test();
protected:
	std::string getTestName();
private:
	bool test001();
	bool test002();
	bool test003();
	bool test004();
	bool test101();

	template<typename ForwardIterator1, typename UnaryPredicate> bool testSameResultsPred(ForwardIterator1 first, ForwardIterator1 last, UnaryPredicate pred) {
		auto stdRes = std::find_if_not(first, last, pred);
		auto pmstlRes = pmstl::find_if_not(first, last, pred);
		return pmstlRes == stdRes;
	}
};