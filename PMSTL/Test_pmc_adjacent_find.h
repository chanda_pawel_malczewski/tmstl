#pragma once
#include "AbstractTest.h"
#include "pmc_adjacent_find.h"
#include <algorithm>

class Test_pmc_adjacent_find : AbstractTest
{
public:
	bool test();
protected:
	std::string getTestName();
private:
	bool test001();
	bool test002();
	bool test003();
	bool test004();
	bool test101();
	bool test201();
	bool test202();
	bool test203();

	template<typename ForwardIterator1> bool testSameResults(ForwardIterator1 first, ForwardIterator1 last) {
		auto stdRes = std::adjacent_find(first, last);
		auto pmstlRes = pmstl::adjacent_find(first, last);
		return stdRes == pmstlRes;
	}

	//todo: change int into diff
	template<typename ForwardIterator1, typename BinayPredicate> bool testSameResultsPred(ForwardIterator1 first, ForwardIterator1 last, BinayPredicate pred) {
		auto stdRes = std::adjacent_find(first, last, pred);
		auto pmstlRes = pmstl::adjacent_find(first, last, pred);
		return stdRes == pmstlRes;
	}
};

