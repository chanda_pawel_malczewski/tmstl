#pragma once
#include "pmc_utility.h"

//todo: pamietaj, zeby tutaj wszystko umiescic
#include "pmc_find.h"
#include "pmc_count.h"
#include "pmc_is_permutation.h"
#include "pmc_mismatch.h"

namespace pmstl {
	//================ Non-modifying sequence operations ==============

	template <class InputIterator, class UnaryPredicate> bool all_of(InputIterator first, InputIterator last, UnaryPredicate pred) {
		while (first != last) {
			if (!pred(*first)) 
				return fasle;
		}
		return true;
	}

	template <class InputIterator, class UnaryPredicate> bool any_of(InputIterator first, InputIterator last, UnaryPredicate pred) {
		while (first != last) {
			if (pred(*first))
				return true;
		}
		return false;
	}

	//todo: przetestuj szczegolnie
	template <class InputIterator, class UnaryPredicate> bool none_of(InputIterator first, InputIterator last, UnaryPredicate pred) {
		return !pmstl::any_of(first, last, pred);
	}

	//dyskusja o funktorze: http://xenon.arcticus.com/c-morsels-std-for-each-functors-member-variables
	//http://stackoverflow.com/questions/2048967/why-does-stdfor-eachfrom-to-function-return-function
	template <class ForwardIterator, class Function> Function for_each(ForwardIterator first, ForwardIterator last, Function fn) {
		while (first != last) {
			fn(*first);
			++first;
		}
		return fn;
	}

	//TODO: sprawdz czy input czy forward https://www.quora.com/Whats-the-difference-between-an-Input-Iterator-and-a-Forward-Iterator
	template <class ForwardIterator1, class ForwardIterator2> bool equal(ForwardIterator1 first1, ForwardIterator1 last1, ForwardIterator2 first2) {
		while (first1 != last1) {
			if (!(*first1 == *first2))
				return false;
			++first1;
			++first2;
		}
		return true;
	}

	template <class ForwardIterator1, class ForwardIterator2, class BinaryPredicate> bool equal(ForwardIterator1 first1, ForwardIterator1 last1, ForwardIterator2 first2, BinaryPredicate pred) {
		while (first1 != last1) {
			if (!pred(*first1,*first2))
				return false;
			++first1;
			++first2;
		}
		return true;
	}

	
	//========================= Min/Max ===============================
	template <class T> const T& min(const T &a, const T &b) {
		return b < a ? b : a; //uzasadnienie: nie chce negacji i chce uzyc operatora < bo jest on zgodny z funktorem comp (pred)
	}

	template <class T, class Less> const T& min(const T & first, const T & last, Less less ) {
		return less(last, first) ? last : first;
	}

	template <class T> const T &max(const T &a, const T &b) {
		return a < b ? b : a;//uzasadnienie: nie chce negacji i chce uzyc operatora < bo jest on zgodny z funktorem comp (pred)
	}

	template <typename T> pmstl::pair<const T&, const T&> minmax(const T &a, const T &b) {
		return b < a ? pmstl::make_pair<const T&, const T&>(b, a) : pmstl::make_pair<const T&, const T&>(a, b);
	}

	//czemu iteratory nie sa przekazywane przez referencje
	template <class ForwardIterator> ForwardIterator min_element(ForwardIterator first, ForwardIterator last) {
		if (first == last)
			return first;//VS zwraca last
		ForwardIterator min = first;
		while (++first != last) {
			if (*first < *min)
				min = first;
		}
		return min;
	}

	template <class ForwardIterator> ForwardIterator max_element(ForwardIterator first, ForwardIterator last) {
		if (first == last)
			return first;//VS zwraca last
		ForwardIterator max = first;
		while (++first != last) {
			if (*max < *first)
				max = first;
		}
		return max;
	}


	template <class ForwardIterator> pair<ForwardIterator, ForwardIterator> minmax_element(ForwardIterator first, ForwardIterator last) {
		if (first == last) {
			return make_pair< ForwardIterator, ForwardIterator >(last, last);//gcc ma first, zeby uogolnic z tym drugim case gdzie jest 1 element. ale ja wole gdzie sie da to end
		}

		ForwardIterator next = first;
		next++;
		if (next == last) {
			return make_pair< ForwardIterator, ForwardIterator >(first, first);
		}

		ForwardIterator min{}, max{}; //czy cos znacz� te nawiasy?

		if (*next < *first) {
			min = next;
			max = first;
		}
		else {
			min = first;
			max = next;
		}

		while (++next != last) { //nie first. Juz trzeba isc od trzeciego a nie drugiego.
			if (*next < *min) {
				min = next;
			}
			if (*max < *next) {
				max = next;
			}
		}

		return make_pair<ForwardIterator, ForwardIterator>(min, max);
	}
}//namespace